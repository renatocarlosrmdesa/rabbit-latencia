#!/bin/sh


#create rabbitmq
sudo docker-compose up -d rabbitmq
  
sleep 30

sudo docker exec rabbitmq rabbitmq-plugins enable rabbitmq_management

sudo docker exec rabbitmq rabbitmqctl add_user sender 12345
sudo docker exec rabbitmq rabbitmqctl set_permissions -p / sender ".*" ".*" ".*"
sudo docker exec rabbitmq rabbitmqctl set_user_tags sender administrator

sudo docker exec rabbitmq rabbitmqctl add_user receiver 12345
sudo docker exec rabbitmq rabbitmqctl set_permissions -p / receiver ".*" ".*" ".*"
sudo docker exec rabbitmq rabbitmqctl set_user_tags receiver administrator


sudo docker-compose up -d receiver

sleep 5

sudo docker-compose up -d sender

sleep 5

#sudo docker exec receiver cp out.txt 



