#!/usr/bin/env python
import pika
import time
credentials = pika.PlainCredentials('receiver', '12345')

connection = pika.BlockingConnection(pika.ConnectionParameters(host='rabbitmq', credentials=credentials))
channel = connection.channel()

channel.queue_declare(queue='task_queue', durable=True)

def callback(ch, method, properties, body):
    tempo=time.time()
    print (" [x] Sent:" + body.decode("utf-8"))
    print ("Received:" + repr(tempo))
    lag=tempo - float(body.decode("utf-8"))
    print ("Latencia:"  + repr(lag))
    with open('out.txt', 'a') as f:
       print(lag," ",file=f)
  
    ##time.sleep(body.count(b'.'))
    print (" [x] Done")
    ch.basic_ack(delivery_tag = method.delivery_tag)

channel.basic_qos(prefetch_count=1)
channel.basic_consume(callback,
                      queue='task_queue')
					 
print(' [*] Waiting for messages. To exit press CTRL+C')
channel.start_consuming()	