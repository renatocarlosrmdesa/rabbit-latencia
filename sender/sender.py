#!/usr/bin/env python
import pika
import time
credentials = pika.PlainCredentials('sender', '12345')
connection = pika.BlockingConnection(pika.ConnectionParameters(host = 'rabbitmq',credentials = credentials))
channel = connection.channel()

channel.queue_declare(queue='', durable=True)

for x in range(100):
	message=repr(time.time() )
	channel.basic_publish(exchange='',
                      routing_key='task_queue',
                      body=message)
	

connection.close()